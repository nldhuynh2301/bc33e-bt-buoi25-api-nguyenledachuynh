var dssv = [];

var BASE_URL = "https://62f8b7663eab3503d1da16eb.mockapi.io";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};

var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

var renderTable = function (list) {
  var contentHTML = "";    
  list.forEach(function (item) {
    item = new SinhVien(item.ma, item.ten, item.email, item.hinhAnh, item.matKhau, item.diemToan, item.diemLy, item.diemHoa);
    var trContent = `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td><img src=${item.hinhAnh} style="width:80px" alt="" /></td>
    <td>${item.diemToan}</td>
    <td>${item.diemLy}</td>
    <td>${item.diemHoa}</td>
    <td>${item.tinhDTB()}</td>
  
    <td>
    <button onclick="xoaSinhVien('${item.ma}')" class="btn btn-danger">Xóa</button>

    <button onclick="suaSinhVien('${item.ma}')" class="btn btn-primary">Sửa</button>

    </td>

    
    </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};
var renderDssvService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();

      // console.log(res.data);
      
      
      dssv = res.data;
    //   for (var index = 0; index < dssv.length; index++){
    //     var current = dssv[index];
    //     var sv = new SinhVien(
    //         current.ma,
    //         current.ten,
    //         current.email,
    //         current.matKhau,
    //         current.hinhAnh,
    //         current.diemToan,
    //         current.diemLy,
    //         current.diemHoa,
    //     );
    //     dssv.push(sv);
    // }
      console.log('dssv: ', dssv);
      // dssv.push(sv);
      //   }
      renderTable(dssv);
      console.log("dssv: ", dssv);
    })
    .catch(function (err) {
      tatLoading();

      console.log(err);
    });
};

renderDssvService();
function xoaSinhVien(id) {
  batLoading();
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      //chạy lại renderDssvService để render danh sách mới nhất sau khi delete.
      tatLoading();
      renderDssvService();
      console.log("res: ", res);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function themSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      tatLoading();
      renderDssvService();
      console.log("res: ", res);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
function suaSinhVien(id) {
  console.log("id: ", id);
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      //chạy lại renderDssvService để render danh sách mới nhất sau khi delete.
      tatLoading();
      console.log("res: ", res);
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
function capNhatSV() {
  var dataForm = layThongTinTuForm();
  console.log("dataForm: ", dataForm);
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      tatLoading();
      renderDssvService();
      console.log("res: ", res);
      //response
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
    //loading, response, reject
}
//bài tập thứ 4 tuần sau nộp BT làm lại
// nút cập nhật là PUT trên web mockapi
