var validator ={
    kiemTraRong:function(valueInput,idError, message){
        if(valueInput.trim() == ""){
            document.getElementById(idError).innerText = message;
            return false;
        } else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },

    kiemTraMaSinhVien: function(maSV,dssv){
        var index = dssv.findIndex((sv) => {
            return sv.ma == maSV;
        });
        if(index !== -1){
            document.getElementById("spanMaSV").innerText = "Mã SV đã tồn tại.";
            return false;
        } else {
            document.getElementById("spanMaSV").innerText = "";
            return true;
        }
    },
    kiemTraDoDai: function(valueInput, idError, min, max){
        //5 đến 20
        var inputLength = valueInput.length;
         if (inputLength < 5 || inputLength > 20){
            document.getElementById(idError).innerText=`Độ dài phải từ ${min} - ${max} kí tự`;
            return false;
         } else {
            document.getElementById(idError).innerText = "";
            return true;
         }
    },
    kiemTraChuoiSo: function (valueInput, idError){
        var regex =  /^[0-9]+$/;
        if(regex.test(valueInput)){
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Trường này chỉ được nhập số";
            return false;
         }
    },

};