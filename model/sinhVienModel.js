function SinhVien(ma, ten, email, hinhAnh, matKhau, diemToan, diemLy, diemHoa) {
  this.ma = ma;
  this.ten = ten;
  this.email = email;
  this.hinhAnh = hinhAnh;
  this.matKhau = matKhau;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;

  this.tinhDTB = function () {
    return ((this.diemToan * 1 + this.diemHoa * 1 + this.diemLy * 1) / 3).toFixed(2);
  };
}
